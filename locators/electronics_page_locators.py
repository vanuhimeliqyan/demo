from selenium.webdriver.common.by import By


class ElectronicsPageLocators:
    def __init__(self):
        pass

    electronics_button_x_path = "//*[@id='mainContent']/div[1]/ul/li[3]/a"
    electronics_icon = (By.XPATH, "/html/body/div[3]/div[2]/h1/span")
    electronics_button = (By.XPATH, electronics_button_x_path)
