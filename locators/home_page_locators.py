from selenium.webdriver.common.by import By


class HomePageLocators:
    def __init__(self):
        pass

    ebay_button = (By.XPATH, "//*[@id='gh-logo']")
    search_field = (By.XPATH, "//*[@id='gh-ac']")
