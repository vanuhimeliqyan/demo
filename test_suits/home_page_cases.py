from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.support import expected_conditions as EC
import unittest
from selenium.webdriver.support.wait import WebDriverWait
from strings.home_page_strings import ebay_url
from locators.home_page_locators import HomePageLocators


class HomePageTest(unittest.TestCase):

    def setUp(self):
        chrome_options = webdriver.ChromeOptions()
        chrome_options.add_argument("window-size=1920,1080")
        self.driver = webdriver.Chrome(options=chrome_options, executable_path="./chromedriver")

    def tearDown(self):
        self.driver.close()

    def test_01(self):
        self.driver.get(ebay_url)
        assert WebDriverWait(self.driver, 5).until(EC.visibility_of_element_located(HomePageLocators.ebay_button))
        assert WebDriverWait(self.driver, 5).until(EC.visibility_of_element_located(HomePageLocators.search_field))


if __name__ == "__main__":
    unittest.main()



